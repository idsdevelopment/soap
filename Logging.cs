﻿using System.Collections.ObjectModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Xml;

// ReSharper disable InconsistentNaming

namespace Ids.Soap;

public class ConsoleOutputMessageInspector : IDispatchMessageInspector
{
	private static readonly MemoryCache<string, (DateTimeOffset TimeStamp, string CarrierId, string Message)> ReceiveCorrelationCache = new();
	private static readonly MemoryCache<string, bool>                                                         AlreadySentCache        = new();

	private static readonly List<WebLog> AzureLogQueue = new();

	private static readonly List<(bool Receive, Message Message, object? CorrelationState)> LogQueue = new();

	private static volatile bool Running,
	                             AzureRunning;


	private static void LogMessageToAzure( bool receive, string correlation, DateTimeOffset timestamp, string carriedId, string message )
	{
		bool NeedToRun;

		lock( AzureLogQueue )
		{
			AzureLogQueue.Add( new WebLog
			                   {
				                   CarrierId   = carriedId,
				                   Message     = $"{( receive ? "Receive" : "Send" )} CorrelationId={correlation}\r\n{message}",
				                   LogDateTime = timestamp,
				                   Program     = SoapService.SOAP_VERSION
			                   } );
			NeedToRun = !AzureRunning;

			if( NeedToRun )
				AzureRunning = true;
		}

		if( NeedToRun )
		{
			Tasks.RunVoid( static async () =>
			               {
				               try
				               {
					               var Client = await SoapService.SOAPClient();

					               if( Client is not null )
					               {
						               using( Client )
						               {
							               while( true )
							               {
								               WebLog Packet;

								               lock( AzureLogQueue )
								               {
									               if( AzureLogQueue.Count == 0 )
										               return;

									               Packet = AzureLogQueue[ 0 ];
									               AzureLogQueue.RemoveAt( 0 );
								               }

								               Client.RequestLogSOAP( Packet ).ConfigureAwait( false ).GetAwaiter().GetResult();
							               }
						               }
					               }
				               }
				               finally
				               {
					               AzureRunning = false;
				               }
			               } );
		}
	}

	private static string GetCarrierId( string message )
	{
		var AuthToken = message.Between( "<sch:authToken>", "</sch:authToken>", StringComparison.OrdinalIgnoreCase ) // Ids1 format
		                ?? message.Between( "<authToken>", "</authToken>", StringComparison.OrdinalIgnoreCase );

		if( AuthToken is not null )
		{
			var Parts = AuthToken.Split( new[] {'-'}, StringSplitOptions.RemoveEmptyEntries );

			if( Parts.Length == 3 )
				return Parts[ 0 ];
		}
		return "IdsRoute";
	}

	private static void LogMessage( bool receive, MessageBuffer buffer, object? correlationState )
	{
		var  OriginalMessage = buffer.CreateMessage();
		bool NeedToRun;

		lock( LogQueue )
		{
			LogQueue.Add( ( receive, OriginalMessage, correlationState ) );
			NeedToRun = !Running;

			if( NeedToRun )
				Running = true;
		}

		if( NeedToRun )
		{
			Tasks.RunVoid( () =>
			               {
				               try
				               {
					               while( true )
					               {
						               (bool Receive, Message Message, object? CorrelationState) Packet;

						               lock( LogQueue )
						               {
							               if( LogQueue.Count == 0 )
								               return;

							               Packet = LogQueue[ 0 ];
							               LogQueue.RemoveAt( 0 );
						               }

						               StringBuilder Sb = new();

						               using var Xw = XmlWriter.Create( Sb );
						               Packet.Message.WriteMessage( Xw );
						               Xw.Close();

						               var Message = Sb.ToString();
						               var Now     = DateTimeOffset.UtcNow;

						               var IsGuid = true;

						               if( Packet.CorrelationState is not Guid G )
						               {
							               IsGuid = false;
							               G      = Guid.NewGuid(); // Shouldn't happen
						               }
						               var Correlation = $"{G:B}";

						               static void LogPacket( bool receive, string correlation, (DateTimeOffset TimeStamp, string CarrierId, string Message) packet )
						               {
							               var (TimeStamp, CarrierId, Packet) = packet;
							               LogMessageToAzure( receive, correlation, TimeStamp, CarrierId, Packet );
						               }

						               var CarrierId = GetCarrierId( Message );

						               if( Packet.Receive )
						               {
							               ReceiveCorrelationCache.AddAbsolute( Correlation, ( Now, CarrierId, Message ), TimeSpan.FromMinutes( 10 ), ( correlation, packet ) =>
							                                                                                                                          {
								                                                                                                                          // Probably an error
								                                                                                                                          LogPacket( true, $"{correlation}**", packet );
							                                                                                                                          } );
						               }
						               else
						               {
							               string Suffix;

							               var (Found, Value) = ReceiveCorrelationCache.TryGetValue( Correlation );

							               if( Found )
							               {
								               ReceiveCorrelationCache.Remove( Correlation );
								               AlreadySentCache.AddAbsolute( Correlation, true, TimeSpan.FromMinutes( 10 ) );

								               LogPacket( true, Correlation, Value );
								               CarrierId = Value.CarrierId;

								               Suffix = "";
							               }
							               else
								               Suffix = AlreadySentCache.ContainsKey( Correlation ) ? "@@@" : "!";

							               if( !IsGuid )
								               Suffix += "!!";

							               LogPacket( false, $"{Correlation}{Suffix}", ( Now, CarrierId, Message ) );
						               }
					               }
				               }
				               finally
				               {
					               Running = false;
				               }
			               } );
		}
	}

	public object AfterReceiveRequest( ref Message request, IClientChannel channel, InstanceContext instanceContext )
	{
		var Buffer = request.CreateBufferedCopy( int.MaxValue );
		request = Buffer.CreateMessage();

		var G = Guid.NewGuid();
		LogMessage( true, Buffer, G );

		return G;
	}

	public void BeforeSendReply( ref Message reply, object? correlationState )
	{
		var Buffer = reply.CreateBufferedCopy( int.MaxValue );
		reply = Buffer.CreateMessage();

		LogMessage( false, Buffer, correlationState );
	}
}

[AttributeUsage( AttributeTargets.Class )]
public class SOAPLogBehavior : Attribute, IServiceBehavior
{
	public void AddBindingParameters( ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters )
	{
	}

	public void ApplyDispatchBehavior( ServiceDescription serviceDescription, ServiceHostBase serviceHostBase )
	{
		foreach( var ChannelDispatcher in serviceHostBase.ChannelDispatchers )
		{
			if( ChannelDispatcher is ChannelDispatcher Dispatcher )
			{
				foreach( var EndpointDispatcher in Dispatcher.Endpoints )
				{
					ConsoleOutputMessageInspector Inspector = new();
					EndpointDispatcher.DispatchRuntime.MessageInspectors.Add( Inspector );
				}
			}
		}
	}

	public void Validate( ServiceDescription serviceDescription, ServiceHostBase serviceHostBase )
	{
	}
}