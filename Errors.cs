﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Net.Http;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;

namespace Ids.Soap;

public static class ObjectExceptionExtensions
{
	public static string ObjectToString( this ObjectException e )
	{
		var Text = new StringBuilder();

		var Obj = e.Object;

		if( Obj is string Str )
		{
			Text.Append( $"""
			              {Str}

			              """ );
		}
		else
		{
			Text.Append( $"""
			              Object Type: {Obj.GetType().Name}


			              """ );

			var Entries = Properties.GetValues( Obj );

			foreach( var Entry in Entries )
				Text.Append( $"{Entry.ParentClass}{Entry.Info.Name}: {ValueToString( Entry.Value1 )}\r\n" );
		}
		return Text.ToString();
	}

	private static string ValueToString( object value )
	{
		const string NULL = "null";

		return value switch
		       {
			       null                        => NULL,
			       string Str when Str != NULL => $"\"{Str}\"",
			       DateTime Dt                 => Dt.ToString( "s" ),
			       DateTimeOffset Dto          => Dto.ToString( "s" ),
			       _                           => value.ToString()
		       };
	}
}

public class ObjectException : Exception
{
	public readonly Exception Exception;
	public readonly object    Object;

	public readonly string Server,
	                       ObjectDescription;

	public ObjectException( Exception e, string server, object? dumpObject, string objectDescription ) : base( e.Message )
	{
		Exception         = e;
		Server            = server;
		Object            = dumpObject ?? "Object is null";
		ObjectDescription = objectDescription;
	}

	public ObjectException( Exception e, HttpClient? server, object? dumpObject ) : this( e,
	                                                                                      server is not null ? server.BaseAddress.AbsoluteUri : "Client is null",
	                                                                                      dumpObject,
	                                                                                      "Client" )
	{
	}
}

public class UnknownException : ObjectException
{
	public UnknownException( Exception e, string server, object? dumpObject, string objectDescription ) : base( e, server, dumpObject, objectDescription )
	{
	}
}

public class TestException : Exception
{
	public TestException() : base( "Test Exception" )
	{
	}
}

public class AuthorisationException : Exception
{
	public AuthorisationException( string loginCode ) : base( $"""
	                                                           Authorisation Exception
	                                                           Login Code: {loginCode}
	                                                           """ )
	{
	}
}

[GlobalErrorBehavior( typeof( GlobalErrorHandler ) )]
public partial class SoapService : ISoapService;

/// <summary>
///     from https://www.codeproject.com/Articles/899342/WCF-Global-Exception-Handling
/// </summary>
public class GlobalErrorHandler : IErrorHandler
{
	/// <summary>
	///     The method that's get invoked if any unhandled exception raised in service
	///     Here you can do what ever logic you would like to.
	///     For example logging the exception details
	///     Here the return value indicates that the exception was handled or not
	///     Return true to stop exception propagation and system considers
	///     that the exception was handled properly
	///     else return false to abort the session
	/// </summary>
	/// <param
	///     name="error">
	/// </param>
	/// <returns></returns>
	public static bool LogException( Exception error )
	{
		var Log = new StringBuilder();

		void LogObjectException( ObjectException e )
		{
			Log.Append( $"""
			             Server: {e.Server}
			             Object: {e.ObjectToString()}

			             """ );
		}

		if( error is ObjectException Exception )
		{
			LogObjectException( Exception );
			error = Exception.Exception;
		}
		else if( error.InnerException is ObjectException InnerException )
			LogObjectException( InnerException );

		Log.AppendLine( error.Message )
		   .AppendLine( error.StackTrace );

		for( var Ie = error.InnerException; Ie is not null; Ie = Ie.InnerException )
		{
			var Message = Ie.Message;

			if( Message.IsNotNullOrWhiteSpace() )
				Log.AppendLine( Ie.Message );
		}

		Tasks.RunVoid( async () =>
		               {
			               try
			               {
				               var CrashData = Log.ToString();
				               Debug.WriteLine( CrashData );

				               await Azure.Client.RequestSoapError( new CrashReport { Account = SoapService.SOAP_VERSION, User = SoapService.SOAP_VERSION, TimeZone = (sbyte)TimeZone.CurrentTimeZone.GetUtcOffset( DateTime.Now ).Hours, CrashData = CrashData } );
			               }
			               catch
			               {
			               }
		               } );
		return true;
	}

	public bool HandleError( Exception error ) => LogException( error );

	/// <summary>
	///     If you want to communicate the exception details to the service client
	///     as proper fault message
	///     here is the place to do it
	///     If we want to suppress the communication about the exception,
	///     set fault to null
	/// </summary>
	/// <param
	///     name="error">
	/// </param>
	/// <param
	///     name="version">
	/// </param>
	/// <param
	///     name="fault">
	/// </param>

	// ReSharper disable once RedundantAssignment
	public void ProvideFault( Exception error, MessageVersion version, ref Message fault )
	{
		var NewEx    = new FaultException( error.Message );
		var MsgFault = NewEx.CreateMessageFault();
		fault = Message.CreateMessage( version, MsgFault, NewEx.Action );
	}
}

public class GlobalErrorBehaviorAttribute : Attribute, IServiceBehavior
{
	private readonly Type ErrorHandlerType;

	/// <summary>
	///     Dependency injection to dynamically inject error handler
	///     if we have multiple global error handlers
	/// </summary>
	/// <param
	///     name="errorHandlerType">
	/// </param>
	public GlobalErrorBehaviorAttribute( Type errorHandlerType )
	{
		ErrorHandlerType = errorHandlerType;
	}

#region IServiceBehavior Members
	void IServiceBehavior.Validate( ServiceDescription description,
	                                ServiceHostBase serviceHostBase )
	{
	}

	void IServiceBehavior.AddBindingParameters( ServiceDescription description,
	                                            ServiceHostBase serviceHostBase,
	                                            Collection<ServiceEndpoint> endpoints,
	                                            BindingParameterCollection parameters )
	{
	}

	/// <summary>
	///     Registering the instance of global error handler in
	///     dispatch behavior of the service
	/// </summary>
	/// <param
	///     name="description">
	/// </param>
	/// <param
	///     name="serviceHostBase">
	/// </param>
	void IServiceBehavior.ApplyDispatchBehavior( ServiceDescription description,
	                                             ServiceHostBase serviceHostBase )
	{
		IErrorHandler ErrorHandler;

		try
		{
			ErrorHandler = (IErrorHandler)Activator.CreateInstance( ErrorHandlerType );
		}
		catch( MissingMethodException E )
		{
			throw new ArgumentException( "The errorHandlerType specified in the ErrorBehaviorAttribute constructor must have a public empty constructor.", E );
		}
		catch( InvalidCastException E )
		{
			throw new ArgumentException( "The errorHandlerType specified in the ErrorBehaviorAttribute constructor must implement System.ServiceModel.Dispatcher.IErrorHandler.", E );
		}

		foreach( var ChannelDispatcherBase in serviceHostBase.ChannelDispatchers )
		{
			var ChannelDispatcher = ChannelDispatcherBase as ChannelDispatcher;
			ChannelDispatcher?.ErrorHandlers.Add( ErrorHandler );
		}
	}
}
#endregion IServiceBehavior Members