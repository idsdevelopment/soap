﻿using System;
using System.Globalization;
using System.ServiceModel;
using SoapTest.Ids1ServiceReference;
using SoapTest.IdsSoapService;
using remoteTripDetailed = SoapTest.Ids1ServiceReference.remoteTripDetailed;

namespace SoapTest
{
	internal class Program
	{
		private static void Main( string[] args )
		{
		#if TERRY
			const string CREDENTIALS = "Terry-Admin-admin";
		#elif IDS1
			const string CREDENTIALS = "Pmltest-test-Test";
		#else
			const string CREDENTIALS = "Pmltest-Admin-admin";
		#endif
			using var AzureSoap = new SoapServiceClient();
			AzureSoap.InnerChannel.OperationTimeout = new TimeSpan( 0, 0, 5, 0 );

			using var IdsSoap = new DSWSClient( new BasicHttpBinding( BasicHttpSecurityMode.None ),
			                                    new EndpointAddress( "http://oz.internetdispatcher.org:8080/ids-beans/IDSWS" ) );

			IdsSoap.InnerChannel.OperationTimeout = new TimeSpan( 0, 0, 5, 0 );

			//				var Trips = Soap.pmlFindTripsDetailedUpdatedSince( CREDENTIALS, "", "", 1, 10, "Return", 86400 * 5 );

			/*				foreach( var Trip in Trips )
							{
								var T = Soap.updateTripDetailedQuick( CREDENTIALS, Trip, false );
							}

							Trips = Soap.pmlFindTripsDetailedUpdatedSince( CREDENTIALS, "ALL", "ALL", 1, 10, "Return", 86400 * 5 );

							foreach( var Trip in Trips )
							{
								var T = Soap.updateTripDetailedQuick( CREDENTIALS, Trip, false );
							}
			*/
			do
			{
				var Date = DateTime.Now;

				var Rnd = new Random();

				var TripId = $"TRW{Rnd.Next( 0, 999999999 ):D9}{Rnd.Next( 0, 99999 ):D5}";

				var RemoteTrip = new remoteTripDetailed
				                 {
					                 //				                 DiagnosticsDataDump = false,
					                 resellerId = "Pmltest",

					                 tripId = TripId,
					                 status = 2,

					                 accountId = "PMLAust",

					                 clientReference = $"931070490979{Rnd.Next( 10 ):D1}",
					                 packageType     = "Terry's Purple Leaf",
					                 serviceLevel    = "Return",

					                 pieces = 1,
					                 weight = 1,

					                 billingNotes = $"AURT{Rnd.Next( 999999 + 1 ):D6}",
					                 board        = 0,
					                 caller       = "caller",
					                 callerEmail  = "caller email",
					                 callerPhone  = "caller phone",
					                 callTakerId  = "calltaker id",

					                 callTime = Date,

					                 //									isCallTimeSpecified = true,

					                 carChargeAmount = 1.0f,
					                 currentZone     = "current zone",

					                 deadlineTime          = Date,
					                 deadlineTimeSpecified = true,

					                 declaredValueAmount = 1.0f,

					                 deliveredTime          = Date,
					                 deliveredTimeSpecified = true,

					                 deliveryAccountId = "PMLAust",
					                 deliveryAddressId = "",
					                 deliveryAmount    = 1.0f,

					                 deliveryArriveTime          = Date,
					                 deliveryArriveTimeSpecified = true,

					                 deliveryCompany     = "MELBOURNE WAREHOUSE (MelWare)",
					                 deliveryCity        = "",
					                 deliveryContact     = "",
					                 deliveryCountry     = "",
					                 deliveryDescription = "",
					                 deliveryEmail       = "",
					                 deliveryNotes       = "",
					                 deliveryPc          = "",
					                 deliveryPhone       = "",
					                 deliveryProvince    = "",
					                 deliveryResellerId  = "",
					                 deliveryStreet      = "",
					                 deliverySuite       = "",
					                 deliveryZone        = "",
					                 disbursementAmount  = 1.0f,
					                 discountAmount      = 1.0f,

					                 dontFinalizeDate          = Date,
					                 dontFinalizeDateSpecified = true,

					                 driver = "",

					                 driverAssignTime          = Date,
					                 driverAssignTimeSpecified = true,

					                 driverPaidCommissionDate          = Date,
					                 driverPaidCommissionDateSpecified = true,

					                 height          = 1.0f,
					                 insuranceAmount = 1.0f,

					                 invoiceId  = 0,
					                 internalId = "",

					                 isCashTrip           = true,
					                 isCashTripReconciled = true,
					                 isDriverPaid         = true,

					                 lastUpdated = DateTime.UtcNow.Ticks,
					                 length      = 1.0f,
					                 miscAmount  = 1.0f,

					                 modified                  = true,
					                 modifiedTripFlagSpecified = true,

					                 modifiedTripDate          = Date,
					                 modifiedTripDateSpecified = true,

					                 noGoodsAmount   = 1.0f,
					                 pallets         = "",
					                 pickupAccountId = "PMLAust",
					                 pickupAddressId = "",

					                 pickupArriveTime          = Date,
					                 pickupArriveTimeSpecified = true,

					                 pickupCompany     = "BURNIE NEWSAGENCY (33164)",
					                 pickupSuite       = "",
					                 pickupStreet      = "18 WILSON ST",
					                 pickupCity        = "BURNIE",
					                 pickupProvince    = "TAS",
					                 pickupPc          = "7320",
					                 pickupCountry     = "Australia",
					                 pickupContact     = "",
					                 pickupDescription = "",
					                 pickupEmail       = "",
					                 pickupNotes       = "",
					                 pickupPhone       = "",
					                 pickupResellerId  = "",
					                 pickupZone        = "",

					                 pickupTime          = Date,
					                 pickupTimeSpecified = true,

					                 podName = "",

					                 priorityInvoiceId          = 1,
					                 priorityInvoiceIdSpecified = true,

					                 priorityStatus          = 1,
					                 priorityStatusSpecified = true,

					                 processedByIdsRouteDate          = Date,
					                 processedByIdsRouteDateSpecified = true,

					                 readyTime          = Date,
					                 readyTimeSpecified = true,

					                 readyTimeString = Date.ToString( CultureInfo.InvariantCulture ),

					                 readyToInvoiceFlag          = true,
					                 readyToInvoiceFlagSpecified = true,

					                 receivedByIdsRouteDate          = Date,
					                 receivedByIdsRouteDateSpecified = true,

					                 redirectAmount     = 1.0f,
					                 sigFilename        = "",
					                 sortOrder          = "0",
					                 stopGroupNumber    = "0",
					                 totalAmount        = 1.0f,
					                 totalFixedAmount   = 1.0f,
					                 totalPayrollAmount = 1.0f,
					                 totalTaxAmount     = 1.0f,
					                 waitTimeAmount     = 1.0f,
					                 waitTimeMins       = 0,
					                 weightAmount       = 1.0f,
					                 weightOrig         = 1.0f,
					                 width              = 1.0f,

					                 skipUpdateCommonAddress = true
				                 };

				try
				{
					//var T = AzureSoap.updateTripDetailedQuick( CREDENTIALS, RemoteTrip, false );
				#if IDS1
					var T = IdsSoap.updateTripDetailedQuick( new updateTripDetailedQuick
					                                         {
						                                         authToken              = CREDENTIALS,
						                                         _tripVal               = RemoteTrip,
						                                         broadcastUpdateMessage = true
					                                         } );
				#endif
				}
				catch( Exception E )

				{
					Console.WriteLine( E );
				}

				Console.WriteLine( "\r\nPress any key for another record\r\nESCAPE to Exit." );
			}
			while( Console.ReadKey().Key != ConsoleKey.Escape );
		}
	}
}