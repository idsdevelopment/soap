﻿using System.Runtime.Serialization;

// ReSharper disable InconsistentNaming

namespace Ids.Soap.RemoteDataTypes;

[DataContract]
[KnownType( typeof( remoteTripDetailedWithAddressNotes ) )]
public class remoteTripDetailed
{
	private const string RETURN = "Return";

#region Diagnostics
	[DataMember]
	public bool DiagnosticsDataDump { get; set; }
#endregion


	//---------- (Compatibility only)----------------
	[DataMember]
	public string internalId { get; set; } = "";

	//-------------------------------------------------------

	[DataMember]
	public string tripId { get; set; } = "";

	[DataMember]
	public string resellerId { get; set; } = "";

	[DataMember]
	public long invoiceId { get; set; }

	[DataMember]
	public bool invoiced { get; set; }

	[DataMember]
	public string serviceLevel { get; set; } = "";

	[DataMember]
	public string packageType { get; set; } = "";

	[DataMember]
	public string callTakerId { get; set; } = "";

	[DataMember]
	public int status { get; set; }

	[DataMember]
	public bool modified { get; set; } // Artifact

	[DataMember]
	public string currentZone { get; set; } = "";

	[DataMember]
	public string accountId { get; set; } = "";

	[DataMember]
	public string caller { get; set; } = "";

	[DataMember]
	public string callerPhone { get; set; } = "";

	[DataMember]
	public string callerEmail { get; set; } = "";

	[DataMember]
	public int pieces { get; set; }

	[DataMember]
	public float weight { get; set; }

	[DataMember]
	public float declaredValueAmount { get; set; }

	[DataMember]
	public string driver { get; set; } = "";

	[DataMember]
	public string billingNotes { get; set; } = "";

	[DataMember]
	public string billingContact { get; set; } = "";

	[DataMember]
	public string billingPhone { get; set; } = "";

	[DataMember]
	public bool returnTrip { get; set; }

	[DataMember]
	public bool dangerousGoods { get; set; }


	[DataMember]
	public string pickupCompany { get; set; } = "";

	[DataMember]
	public string pickupNotes { get; set; } = "";

	[DataMember]
	public string pickupContact { get; set; } = "";

	[DataMember]
	public string pickupPhone { get; set; } = "";

	[DataMember]
	public string pickupZone { get; set; } = "";

	[DataMember]
	public string pickupEmail { get; set; } = "";

	[DataMember]
	public string pickupAddressId { get; set; } = "";

	[DataMember]
	public string pickupResellerId { get; set; } = "";

	[DataMember]
	public string pickupAccountId { get; set; } = "";

	[DataMember]
	public string pickupDescription { get; set; } = "";

	[DataMember]
	public string pickupSuite { get; set; } = "";

	[DataMember]
	public string pickupStreet { get; set; } = "";

	[DataMember]
	public string pickupCity { get; set; } = "";

	[DataMember]
	public string pickupProvince { get; set; } = "";

	[DataMember]
	public string pickupPc { get; set; } = "";

	[DataMember]
	public string pickupCountry { get; set; } = "";

	[DataMember]
	public string deliveryCompany { get; set; } = "";

	[DataMember]
	public string deliveryNotes { get; set; } = "";

	[DataMember]
	public string deliveryContact { get; set; } = "";

	[DataMember]
	public string deliveryPhone { get; set; } = "";

	[DataMember]
	public string deliveryZone { get; set; } = "";

	[DataMember]
	public string deliveryEmail { get; set; } = "";

	[DataMember]
	public string deliveryAddressId { get; set; } = "";

	[DataMember]
	public string deliveryResellerId { get; set; } = "";

	[DataMember]
	public string deliveryAccountId { get; set; } = "";

	[DataMember]
	public string deliveryDescription { get; set; } = "";

	[DataMember]
	public string deliverySuite { get; set; } = "";

	[DataMember]
	public string deliveryStreet { get; set; } = "";

	[DataMember]
	public string deliveryCity { get; set; } = "";

	[DataMember]
	public string deliveryProvince { get; set; } = "";

	[DataMember]
	public string deliveryPc { get; set; } = "";

	[DataMember]
	public string deliveryCountry { get; set; } = "";

	[DataMember]
	public float deliveryAmount { get; set; }

	[DataMember]
	public float miscAmount { get; set; }

	[DataMember]
	public float insuranceAmount { get; set; }

	[DataMember]
	public float weightAmount { get; set; }


	[DataMember]
	public int waitTimeMins { get; set; }

	[DataMember]
	public float waitTimeAmount { get; set; }

	[DataMember]
	public float carChargeAmount { get; set; }

	[DataMember]
	public float redirectAmount { get; set; }

	[DataMember]
	public bool skipUpdateCommonAddress { get; set; }

	[DataMember]
	public float noGoodsAmount { get; set; }

	[DataMember]
	public float disbursementAmount { get; set; }

	[DataMember]
	public float totalAmount { get; set; }

	[DataMember]
	public float totalTaxAmount { get; set; }

	[DataMember]
	public float discountAmount { get; set; }

	[DataMember]
	public float totalFixedAmount { get; set; }

	[DataMember]
	public bool POD { get; set; }

	[DataMember]
	public bool mailDrop { get; set; }

	[DataMember]
	public string readyTimeString { get; set; } = "";

	[DataMember]
	public string clientReference { get; set; } = "";

	[DataMember]
	public bool isDriverPaid { get; set; }

	[DataMember]
	public bool isCashTrip { get; set; }

	[DataMember]
	public bool isCashTripReconciled { get; set; }

	[DataMember]
	public string podName { get; set; } = "";


	[DataMember]
	public long lastUpdated { get; set; }

	[DataMember]
	public float totalPayrollAmount { get; set; }

	[DataMember]
	public string pallets { get; set; } = "";

	[DataMember]
	public float length { get; set; }

	[DataMember]
	public float width { get; set; }

	[DataMember]
	public float height { get; set; }

	[DataMember]
	public float weightOrig { get; set; }

	[DataMember]
	public string sigFilename { get; set; } = "";

	[DataMember]
	public int board { get; set; }

	[DataMember]
	public bool disputed { get; set; }

	[DataMember]
	public bool carCharge { get; set; }

	[DataMember]
	public bool redirect { get; set; }

	[DataMember]
	public string sortOrder { get; set; } = "";


	[DataMember]
	public string stopGroupNumber { get; set; } = "";


	internal virtual TripUpdate AsTripUpdate
	{
		get
		{
			var (DAdr1, DAdr2)     = SplitAddress( deliveryStreet );
			var (PAdr1, PAdr2)     = SplitAddress( pickupStreet );
			var (Status1, Status2) = status.FromIds1Status();
			var (PuCo, PuLoc)      = SplitCompany( pickupCompany );
			var (DelCo, DelLoc)    = SplitCompany( deliveryCompany );
			var SLevel = serviceLevel.Trim();

			string  Pop, Pod;
			STATUS2 Status3;

			if( Status1 <= Protocol.Data.STATUS.PICKED_UP )
			{
				Pop     = podName;
				Pod     = "";
				Status3 = STATUS2.DONT_UPDATE_POD;
			}
			else
			{
				Pod     = podName;
				Pop     = "";
				Status3 = STATUS2.DONT_UPDATE_POP;
			}

			var DeliveryTime = deliveredTime.AsPacificStandardTime();

			var Temp = new TripUpdate
			           {
				           Program   = SoapService.SOAP_VERSION,
				           TripId    = tripId.NullTrim(),
				           AccountId = accountId.NullTrim(),

				           CallTime            = callTime.AsPacificStandardTime(),
				           IsCallTimeSpecified = isCallTimeSpecified,

				           Board = board.ToString(),

				           DeliveryTime          = DeliveryTime,
				           VerifiedTime          = DeliveryTime,
				           DeliveryTimeSpecified = deliveredTimeSpecified,

				           DueTime          = deadlineTime.AsPacificStandardTime(),
				           DueTimeSpecified = deadlineTimeSpecified,

				           PickupTime          = pickupTime.AsPacificStandardTime(),
				           PickupTimeSpecified = pickupTimeSpecified,

				           ReadyTime          = readyTime.AsPacificStandardTime(),
				           ReadyTimeSpecified = readyTimeSpecified,

				           Driver         = driver.NullTrim(),
				           CallerEmail    = callerEmail.NullTrim(),
				           CallerName     = caller.NullTrim(),
				           CallerPhone    = callerPhone.Max50(),
				           CurrentZone    = currentZone.IsNotNullOrEmpty() ? currentZone : pickupZone,
				           DangerousGoods = dangerousGoods,

				           BillingNotes        = billingNotes.NullTrim(),
				           BillingAddressPhone = billingPhone.NullTrim(),
				           BillingContact      = billingContact.NullTrim(),

				           PickupNotes   = pickupNotes.NullTrim(),
				           DeliveryNotes = deliveryNotes.NullTrim(),

				           DeliveryCompanyName         = DelCo.NullTrim(),
				           DeliveryAddressBarcode      = DelLoc.NullTrim(),
				           DeliveryAccountId           = deliveryAccountId,
				           DeliveryAddressAddressLine1 = DAdr1.NullTrim(),
				           DeliveryAddressAddressLine2 = DAdr2.NullTrim(),
				           DeliveryAddressCity         = deliveryCity.NullTrim(),
				           DeliveryAddressCountry      = deliveryCountry.NullTrim(),
				           DeliveryAddressEmailAddress = deliveryEmail.NullTrim(),
				           DeliveryAddressPhone        = deliveryPhone.Max20(),
				           DeliveryAddressPostalCode   = deliveryPc.NullTrim(),
				           DeliveryAddressRegion       = deliveryProvince.NullTrim(),
				           DeliveryAddressSuite        = deliverySuite.NullTrim(),
				           DeliveryContact             = deliveryContact.NullTrim(),
				           DeliveryZone                = deliveryZone.NullTrim(),

				           PickupCompanyName         = PuCo.NullTrim(),
				           PickupAddressBarcode      = PuLoc.NullTrim(),
				           PickupAccountId           = pickupAccountId.NullTrim(),
				           PickupAddressAddressLine1 = PAdr1.NullTrim(),
				           PickupAddressAddressLine2 = PAdr2.NullTrim(),
				           PickupAddressCity         = pickupCity.NullTrim(),
				           PickupAddressCountry      = pickupCountry.NullTrim(),
				           PickupAddressEmailAddress = pickupEmail.NullTrim(),
				           PickupAddressPhone        = pickupPhone.Max20(),
				           PickupAddressPostalCode   = pickupPc.NullTrim(),
				           PickupAddressRegion       = pickupProvince.NullTrim(),
				           PickupAddressSuite        = pickupSuite.NullTrim(),
				           PickupContact             = pickupContact.NullTrim(),
				           PickupZone                = pickupZone.NullTrim(),

				           Status1 = Status1,
				           Status2 = Status2,
				           Status3 = Status3,

				           Reference    = clientReference.NullTrim(),
				           ServiceLevel = SLevel,
				           PackageType  = packageType.NullTrim(),
				           Pieces       = pieces,
				           Weight       = (decimal)weight,

				           Packages = new List<TripPackage>(),

				           PickupAddressNotes   = "",
				           DeliveryAddressNotes = "",
				           BillingAddressNotes  = "",

				           POP = Pop,
				           POD = Pod,

				           ReceivedByDevice = carCharge,
				           ReadByDriver     = redirect,

				           CallTakerId = callTakerId
			           };

			return Temp;
		}
	}

	internal TripUpdate AsPmlTrip
	{
		get
		{
			var Result = AsTripUpdate;

			var SLevel = serviceLevel.NullTrim();

			Result.ServiceLevel = SLevel.StartsWith( "Up", StringComparison.OrdinalIgnoreCase ) ? "Up" : "Return";

			Result.Packages.Add( new TripPackage
			                     {
				                     PackageType = RETURN,
				                     Pieces      = pieces,
				                     Weight      = (decimal)weight,
				                     Items = new List<TripItem>
				                             {
					                             new()
					                             {
						                             Barcode     = clientReference.NullTrim(),
						                             ItemCode    = SLevel.NullTrim(),
						                             Description = packageType.NullTrim(),
						                             Pieces      = pieces,
						                             Weight      = (decimal)weight
					                             }
				                             }
			                     }
			                   );
			return Result;
		}
	}

	public remoteTripDetailed()
	{
	}

	public remoteTripDetailed( TripUpdate t )
	{
		tripId       = t.TripId;
		serviceLevel = t.ServiceLevel;
		packageType  = t.PackageType;
		status       = t.ToStatus();
		currentZone  = t.CurrentZone;
		accountId    = t.AccountId;
		pieces       = (int)t.Pieces;
		weight       = (float)t.Weight;

		if( !int.TryParse( t.Board, out var Board ) )
			Board = 0;

		board = Board;

		callTime            = t.CallTime?.DateTime;
		isCallTimeSpecified = t.IsCallTimeSpecified;

		readyTime          = t.ReadyTime?.DateTime;
		readyTimeSpecified = t.ReadyTimeSpecified;

		deadlineTime          = t.DueTime?.DateTime;
		deadlineTimeSpecified = t.DueTimeSpecified;

		pickupTime          = t.PickupTime?.DateTime;
		pickupTimeSpecified = t.PickupTimeSpecified;

		pickupArriveTime          = t.PickupTime?.DateTime;
		pickupArriveTimeSpecified = t.PickupTimeSpecified;

		deliveredTime          = t.DeliveryTime?.DateTime;
		deliveredTimeSpecified = t.DeliveryTimeSpecified;

		driver       = t.Driver;
		billingNotes = t.BillingAddressNotes;

		pickupAccountId   = t.PickupAccountId;
		pickupPhone       = t.PickupAddressPhone;
		pickupZone        = t.PickupZone;
		pickupCompany     = t.PickupCompanyName;
		pickupContact     = t.PickupContact;
		pickupNotes       = t.PickupAddressNotes;
		pickupEmail       = t.PickupAddressEmailAddress;
		pickupCity        = t.PickupAddressCity;
		pickupCountry     = t.PickupAddressCountry;
		pickupPc          = t.PickupAddressPostalCode;
		pickupProvince    = t.PickupAddressRegion;
		pickupStreet      = $"{t.PickupAddressAddressLine1}\r\n{t.PickupAddressAddressLine2}";
		pickupSuite       = t.PickupAddressSuite;
		pickupDescription = t.PickupNotes;

		deliveryAccountId   = t.DeliveryAccountId;
		deliveryPhone       = t.DeliveryAddressPhone;
		deliveryZone        = t.DeliveryZone;
		deliveryCompany     = t.DeliveryCompanyName;
		deliveryContact     = t.DeliveryContact;
		deliveryNotes       = t.DeliveryAddressNotes;
		deliveryEmail       = t.DeliveryAddressEmailAddress;
		deliveryCity        = t.DeliveryAddressCity;
		deliveryCountry     = t.DeliveryAddressCountry;
		deliveryPc          = t.DeliveryAddressPostalCode;
		deliveryProvince    = t.DeliveryAddressRegion;
		deliveryStreet      = $"{t.DeliveryAddressAddressLine1}\r\n{t.DeliveryAddressAddressLine2}";
		deliverySuite       = t.DeliveryAddressSuite;
		deliveryDescription = t.DeliveryNotes;

		podName = t.POD;

		caller      = t.CallerName;
		callerEmail = t.CallerEmail;
		callerPhone = t.CallerPhone;

		clientReference = t.Reference;
		dangerousGoods  = t.DangerousGoods;

		carCharge = t.ReceivedByDevice;
		redirect  = t.ReadByDriver;

		callTakerId = t.CallTakerId;
	}

	private static (string Adr1, string Adr2) SplitAddress( string adr )
	{
		var Lines = adr.Replace( '\r', '\n' ).Replace( "\n\n", "\n" ).Split( new[] {'\n'}, StringSplitOptions.RemoveEmptyEntries );

		return Lines.Length switch
		       {
			       0 => ( "", "" ),
			       1 => ( Lines[ 0 ], "" ),
			       _ => ( Lines[ 0 ], Lines[ 1 ] )
		       };
	}

	private static (string CoName, string Location) SplitCompany( string coName )
	{
		coName = coName.Trim();

		var Result = ( CoName: coName, Location: "" );

		var P2 = coName.Length;

		if( ( P2-- > 3 ) && ( coName[ P2 ] == ')' ) ) //  (?)
		{
			var P = coName.LastIndexOf( '(' );

			if( ( P >= 0 ) && ( P2 > P ) )
			{
				Result.Location = coName.SubStr( P + 1, P2 - P - 1 );
				//	Result.CoName   = coName.SubStr( 0, P );
			}
		}
		return Result;
	}

#region Optional Fields
	[DataMember( IsRequired = false )]
	public DateTime? callTime { get; set; }

	[DataMember( IsRequired = true )]
	public bool isCallTimeSpecified { get; set; }


	[DataMember( IsRequired = false )]
	public DateTime? deliveredTime { get; set; }

	[DataMember( IsRequired = true )]
	public bool deliveredTimeSpecified { get; set; }


	[DataMember( IsRequired = false )]
	public DateTime? deadlineTime { get; set; }

	[DataMember( IsRequired = true )]
	public bool deadlineTimeSpecified { get; set; }


	[DataMember( IsRequired = false )]
	public DateTime? deliveryArriveTime { get; set; }

	[DataMember( IsRequired = true )]
	public bool deliveryArriveTimeSpecified { get; set; }


	[DataMember( IsRequired = false )]
	public DateTime? dontFinalizeDate { get; set; }

	[DataMember( IsRequired = true )]
	public bool dontFinalizeDateSpecified { get; set; }


	[DataMember( IsRequired = false )]
	public bool? dontFinalizeFlag { get; set; }

	[DataMember( IsRequired = true )]
	public bool dontFinalizeFlagSpecified { get; set; }


	[DataMember( IsRequired = false )]
	public DateTime? driverAssignTime { get; set; }

	[DataMember( IsRequired = true )]
	public bool driverAssignTimeSpecified { get; set; }


	[DataMember( IsRequired = false )]
	public DateTime? driverPaidCommissionDate { get; set; }

	[DataMember( IsRequired = true )]
	public bool driverPaidCommissionDateSpecified { get; set; }


	[DataMember( IsRequired = false )]
	public DateTime? modifiedTripDate { get; set; }

	[DataMember( IsRequired = true )]
	public bool modifiedTripDateSpecified { get; set; }


	[DataMember( IsRequired = false )]
	public bool? modifiedTripFlag { get; set; }

	[DataMember( IsRequired = true )]
	public bool modifiedTripFlagSpecified { get; set; }


	[DataMember( IsRequired = false )]
	public DateTime? pickupArriveTime { get; set; }

	[DataMember( IsRequired = true )]
	public bool pickupArriveTimeSpecified { get; set; }


	[DataMember( IsRequired = false )]
	public DateTime? pickupTime { get; set; }

	[DataMember( IsRequired = true )]
	public bool pickupTimeSpecified { get; set; }


	[DataMember( IsRequired = false )]
	public long? priorityInvoiceId { get; set; }

	[DataMember( IsRequired = true )]
	public bool priorityInvoiceIdSpecified { get; set; }


	[DataMember( IsRequired = false )]
	public int? priorityStatus { get; set; }

	[DataMember( IsRequired = true )]
	public bool priorityStatusSpecified { get; set; }


	[DataMember( IsRequired = false )]
	public DateTime? processedByIdsRouteDate { get; set; }

	[DataMember( IsRequired = true )]
	public bool processedByIdsRouteDateSpecified { get; set; }


	[DataMember( IsRequired = false )]
	public DateTime? readyTime { get; set; }

	[DataMember( IsRequired = true )]
	public bool readyTimeSpecified { get; set; }


	[DataMember( IsRequired = false )]
	public bool? readyToInvoiceFlag { get; set; }

	[DataMember( IsRequired = true )]
	public bool readyToInvoiceFlagSpecified { get; set; }


	[DataMember( IsRequired = false )]
	public DateTime? receivedByIdsRouteDate { get; set; }

	[DataMember( IsRequired = true )]
	public bool receivedByIdsRouteDateSpecified { get; set; }
#endregion
}