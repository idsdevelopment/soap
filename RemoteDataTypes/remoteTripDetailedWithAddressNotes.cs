﻿using System.Runtime.Serialization;

// ReSharper disable InconsistentNaming

namespace Ids.Soap.RemoteDataTypes;

[DataContract]
public class remoteTripDetailedWithAddressNotes : remoteTripDetailed
{
	[DataMember]
	public string pickupAddressNotes { get; set; } = "";

	[DataMember]
	public string deliveryAddressNotes { get; set; } = "";

	[DataMember]
	public string billingAddressNotes { get; set; } = "";

	internal override TripUpdate AsTripUpdate
	{
		get
		{
			var Temp = base.AsTripUpdate;
			Temp.PickupAddressNotes   = pickupAddressNotes.NullTrim();
			Temp.DeliveryAddressNotes = deliveryAddressNotes.NullTrim();
			Temp.BillingAddressNotes  = billingAddressNotes.NullTrim();

			return Temp;
		}
	}
}