﻿namespace Ids.Soap.RemoteDataTypes;

public static class Extensions
{
	public static (Protocol.Data.STATUS Status1, STATUS1 Status2) ToStatus( this STATUS s )
	{
		return s switch
		       {
			       STATUS.ACTIVE      => ( Protocol.Data.STATUS.ACTIVE, STATUS1.UNSET ),
			       STATUS.DELETED     => ( Protocol.Data.STATUS.DELETED, STATUS1.UNSET ),
			       STATUS.DELIVERED   => ( Protocol.Data.STATUS.DELIVERED, STATUS1.UNSET ),
			       STATUS.DISPATCHED  => ( Protocol.Data.STATUS.DISPATCHED, STATUS1.UNSET ),
			       STATUS.NEW         => ( Protocol.Data.STATUS.NEW, STATUS1.UNSET ),
			       STATUS.PICKED_UP   => ( Protocol.Data.STATUS.PICKED_UP, STATUS1.UNSET ),
			       STATUS.POSTED      => ( Protocol.Data.STATUS.POSTED, STATUS1.UNSET ),
			       STATUS.SCHEDULED   => ( Protocol.Data.STATUS.SCHEDULED, STATUS1.UNSET ),
			       STATUS.UNDELIVERED => ( Protocol.Data.STATUS.DELIVERED, STATUS1.UNDELIVERED ),
			       STATUS.VERIFIED    => ( Protocol.Data.STATUS.VERIFIED, STATUS1.UNSET ),
			       _                  => ( Protocol.Data.STATUS.UNSET, STATUS1.UNSET )
		       };
	}

	public static STATUS ToStatus( Protocol.Data.STATUS s, STATUS1 s1 )
	{
		if( s1 == STATUS1.UNDELIVERED )
			return STATUS.UNDELIVERED;

		return s switch
		       {
			       Protocol.Data.STATUS.ACTIVE     => STATUS.ACTIVE,
			       Protocol.Data.STATUS.DELETED    => STATUS.DELETED,
			       Protocol.Data.STATUS.DELIVERED  => STATUS.DELIVERED,
			       Protocol.Data.STATUS.DISPATCHED => STATUS.DISPATCHED,
			       Protocol.Data.STATUS.NEW        => STATUS.NEW,
			       Protocol.Data.STATUS.PICKED_UP  => STATUS.PICKED_UP,
			       Protocol.Data.STATUS.POSTED     => STATUS.POSTED,
			       Protocol.Data.STATUS.SCHEDULED  => STATUS.SCHEDULED,
			       Protocol.Data.STATUS.VERIFIED   => STATUS.VERIFIED,
			       _                               => STATUS.UNSET
		       };
	}

	public static int ToInt( this STATUS s ) => (int)s;

	public static int ToStatus( this TripUpdate t ) => ToStatus( t.Status1, t.Status2 ).ToInt();

	public static STATUS ToStatus( this int i ) => (STATUS)i;

	public static string Max100( this string? str ) => str.NullTrim().MaxLength( 100 );
	public static string Max50( this string? str ) => str.NullTrim().MaxLength( 50 );
	public static string Max20( this string? str ) => str.NullTrim().MaxLength( 20 );
}