﻿using static AzureRemoteService.Azure;

namespace Ids.Soap;

public static class Extensions
{
	public static (bool Ok, string CarrierId, string UserName, string Password) SplitAuthToken( this string authToken )
	{
		var Parts = authToken.Trim().Replace( "--", "-" ).Split( new[] { '-' }, StringSplitOptions.None );

		if( Parts.Length == 3 )
		{
			var CarrierId = Parts[ 0 ].TrimToUpper();
			var UserName  = Parts[ 1 ].TrimToUpper();
			var Password  = Parts[ 2 ];
			return ( true, CarrierId, UserName, Password );
		}
		return ( false, "", "", "" );
	}
}

public partial class SoapService : ISoapService
{
	private const string SOAP_CARRIER  = "ids/idsroute",
	                     SOAP_USERNAME = "SpecialSoapLogin",
	                     SOAP_PASSWORD = "skfjjk;sfljkesflkjsfdlo'jksssaf";

	private const int EXPIRED_CLEANUP_INTERVAL_IN_MINUTES = 10,
	                  AUTH_TIMEOUT_IN_MINUTES             = 2 * 24 * 60; // Auth token times out in 6 days  (Token expires in 2)

	private static readonly MemoryCache<string, InternalClient> LoginCache = new();


	// ReSharper disable once InconsistentNaming
	public static async Task<InternalClient?> SOAPClient()
	{
		var (Status, Client) = await LogInNoGlobal( SOAP_VERSION, SOAP_CARRIER, SOAP_USERNAME, SOAP_PASSWORD );
		return Status == AZURE_CONNECTION_STATUS.OK ? Client : null;
	}

	public static async Task<(bool Ok, string CarrierId, string UserName, InternalClient? Client)> Login( string idsAuthToken )
	{
		var (Ok, CarrierId, UserName, Password) = idsAuthToken.SplitAuthToken();

		if( Ok )
			return await Login( CarrierId, UserName, Password );

		return ( false, "", "", null );
	}


	public static async Task<(bool Ok, string CarrierId, string UserName, InternalClient? Client)> Login( string carrierId, string userName, string password )
	{
		IdsClient.RethrowCommunicationsException = true;

		var WasException = false;

		var Ok = false;

		try
		{
			( Ok, var Client ) = LoginCache[ carrierId ];

			if( !Ok )
			{
				NoListeners = true;

				var (Status, InternalClient) = await LogInNoGlobal( SOAP_VERSION, carrierId, userName, password );

				Ok = Status == AZURE_CONNECTION_STATUS.OK;

				if( Ok )
				{
					LoginCache.AddAbsolute( carrierId, InternalClient, new TimeSpan( 0, AUTH_TIMEOUT_IN_MINUTES, 0 ), ( _, client ) =>
					                                                                                                  {
						                                                                                                  Tasks.RunVoid( EXPIRED_CLEANUP_INTERVAL_IN_MINUTES * 60_1000,
						                                                                                                                 () =>
						                                                                                                                 {
							                                                                                                                 try
							                                                                                                                 {
								                                                                                                                 client?.Dispose();
							                                                                                                                 }
							                                                                                                                 catch
							                                                                                                                 {
							                                                                                                                 }
						                                                                                                                 } );
					                                                                                                  } );

					return ( true, carrierId, userName, InternalClient );
				}
			}
			else
				return ( true, carrierId, userName, Client );
		}
		catch( Exception Exception )
		{
			WasException = true;
			GlobalErrorHandler.LogException( new ObjectException( Exception, Client, new { CarrierId = carrierId, UserName = userName, Password = password } ) );
		}
		finally
		{
			if( !Ok && !WasException )
				GlobalErrorHandler.LogException( new AuthorisationException( $"""
				                                                              {carrierId}
				                                                              {userName}
				                                                              {password}
				                                                              """ ) );
		}
		return ( false, "", "", null );
	}
}