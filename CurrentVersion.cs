﻿namespace Ids.Soap;

public partial class SoapService : ISoapService
{
	public const string BASE_VERSION = "SOAP_V2_0.1";

#if DEBUG
	public const string SOAP_VERSION = $"{BASE_VERSION}_DEBUG";
#else
	public const string SOAP_VERSION = BASE_VERSION;
#endif
}