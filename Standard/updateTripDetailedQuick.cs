﻿using STATUS = Ids.Soap.RemoteDataTypes.STATUS;

#pragma warning disable IDE1006 // Symbol names

// ReSharper disable InconsistentNaming

namespace Ids.Soap;

public partial interface ISoapService
{
	[OperationContract]
	public remoteTripDetailed updateTripDetailedQuick( string authToken, remoteTripDetailed trip, bool broadcastUpdateMessage );
}

public partial class SoapService : ISoapService
{
	public remoteTripDetailed updateTripDetailedQuick( string authToken, remoteTripDetailed trip, bool broadcastUpdateMessage )
	{
		if( trip.status != (int)STATUS.SCHEDULED )
			UpdateQueue.Add( authToken, nameof( remoteTripDetailed ), trip );
		return trip;
	}
}