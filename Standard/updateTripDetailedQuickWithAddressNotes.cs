﻿using STATUS = Ids.Soap.RemoteDataTypes.STATUS;

#pragma warning disable IDE1006 // Symbol names

// ReSharper disable InconsistentNaming

namespace Ids.Soap;

public partial interface ISoapService
{
	[OperationContract]
	public remoteTripDetailedWithAddressNotes updateTripDetailedQuickWithAddressNotes( string authToken, remoteTripDetailedWithAddressNotes trip, bool broadcastUpdateMessage );
}

public partial class SoapService : ISoapService
{
	public remoteTripDetailedWithAddressNotes updateTripDetailedQuickWithAddressNotes( string authToken, remoteTripDetailedWithAddressNotes trip, bool broadcastUpdateMessage )
	{
		if( trip.status != (int)STATUS.SCHEDULED )
			UpdateQueue.Add( authToken, nameof( remoteTripDetailedWithAddressNotes ), trip );
		return trip;
	}
}