﻿using System.Diagnostics;
using System.Threading;
using STATUS = Protocol.Data.STATUS;

namespace Ids.Soap;

public static class UpdateQueue
{
	private const int MAX_RETRIES                   = 60,
	                  RETRY_SLEEP_PERIOD_IN_SECONDS = 30;

	private const string UNKNOWN_OBJECT_TYPE = "Unknown object type";

	private static readonly Dictionary<string, LinkedList<UpdateObject>> QueueObjects = [];
	private static readonly HashSet<string>                              CarrierLocks = [];

	private static readonly object LockObject = new();

	internal enum SPECIAL_CARRIER
	{
		NONE,
		PML,
		LOOPBACK_TEST,
		PHOENIX
	}

	public static void Add( string authToken, string objectDescription, object @object )
	{
		var (Ok, CarrierId, UserName, Password) = authToken.SplitAuthToken();

		if( Ok )
		{
			CarrierId = CarrierId.TrimToUpper(); // For Special carriers, the carrierId is case-sensitive

			lock( LockObject )
			{
				if( !QueueObjects.TryGetValue( CarrierId, out var ObjectList ) )
					QueueObjects[ CarrierId ] = ObjectList = [];

				lock( ObjectList )
					ObjectList.AddLast( new UpdateObject( CarrierId, UserName, Password, objectDescription, @object ) );

				if( CarrierLocks.Add( CarrierId ) )
				{
					Tasks.RunVoid( () =>
					               {
						               ProcessQueue( CarrierId, ObjectList );
					               } );
				}
			}
		}
	}

	private class UpdateObject
	{
		public string CarrierId { get; }
		public string UserName  { get; }
		public string Password  { get; }

		public string ObjectDescription { get; }
		public object Object            { get; }
		public ushort Retries;

		public UpdateObject( string carrierId, string userName, string password, string objectDescription, object @object )
		{
			CarrierId         = carrierId;
			UserName          = userName;
			Password          = password;
			ObjectDescription = objectDescription;
			Object            = @object;
		}
	}

	private static TripUpdate InitTrip( TripUpdate t )
	{
		switch( t.Status1 )
		{
		case < STATUS.DISPATCHED:
			t.BroadcastToDispatchBoard = true;
			t.BroadcastToDriverBoard   = false;
			t.BroadcastToDriver        = false;
			break;

		case <= STATUS.VERIFIED:
			t.BroadcastToDispatchBoard = false;
			t.BroadcastToDriverBoard   = true;
			t.BroadcastToDriver        = true;
			break;

		default:
			t.BroadcastToDispatchBoard = true;
			t.BroadcastToDriverBoard   = true;
			t.BroadcastToDriver        = true;
			break;
		}
		t.Program = SoapService.SOAP_VERSION;
		return t;
	}

	private static async void ProcessQueue( string carrierId, LinkedList<UpdateObject> objects )
	{
		try
		{
			while( true )
			{
				UpdateObject? UpdateObject;

				lock( objects )
				{
					if( objects.Count > 0 )
					{
						UpdateObject = objects.First.Value;
						objects.RemoveFirst();
					}
					else
						break;
				}

				var Server = "";

				try
				{
					var (Ok, _, _, Client) = await SoapService.Login( UpdateObject.CarrierId, UpdateObject.UserName, UpdateObject.Password );

					if( Ok )
					{
						var SpecialCarrier = carrierId switch
						                     {
							                     "PML"          => SPECIAL_CARRIER.PML,
							                     "PMLTEST"      => SPECIAL_CARRIER.PML,
							                     "LOOPBACKTEST" => SPECIAL_CARRIER.LOOPBACK_TEST,
							                     "PHOENIX"      => SPECIAL_CARRIER.PHOENIX,
							                     "PHOENIXTEST"  => SPECIAL_CARRIER.PHOENIX,
							                     _              => SPECIAL_CARRIER.NONE
						                     };

						var C = Client!;
						Server = C.BaseAddress.AbsoluteUri;

						switch( UpdateObject.Object )
						{
						case remoteTripDetailedWithAddressNotes RemoteTripDetailedWithAddressNotes:
							await C.RequestAddUpdateTripAutoCreate( InitTrip( RemoteTripDetailedWithAddressNotes.AsTripUpdate ) );
							break;

						case remoteTripDetailed RemoteTripDetailed:
							// ReSharper disable once ConvertSwitchStatementToSwitchExpression
							switch( SpecialCarrier )
							{
							case SPECIAL_CARRIER.PML:
								_ = await C.RequestPML_updateTripDetailedQuick( InitTrip( RemoteTripDetailed.AsPmlTrip ) );
								break;

							case SPECIAL_CARRIER.PHOENIX:
								_ = await C.RequestAddUpdateTripAutoCreateDontUpdateAddresses( InitTrip( RemoteTripDetailed.AsTripUpdate ) );
								break;

							default:
								_ = await C.RequestAddUpdateTripAutoCreate( InitTrip( RemoteTripDetailed.AsTripUpdate ) );
								break;
							}
							break;

						default:
							throw new UnknownException( new Exception( UNKNOWN_OBJECT_TYPE ),
							                            Server, UpdateObject.Object,
							                            UNKNOWN_OBJECT_TYPE );
						}
					}
					else
						throw new AuthorisationException( $"{UpdateObject.CarrierId}-{UpdateObject.UserName}-{UpdateObject.Password}" );
				}
				catch( Exception Exception )
				{
					Debug.WriteLine( Exception );

					ObjectException ErrorException;

					switch( Exception )
					{
					case UnknownException UnknownException:
						ErrorException = UnknownException;
						break;

					default:
						if( Exception is not AuthorisationException )
						{
							if( UpdateObject.Retries++ < MAX_RETRIES )
							{
								lock( objects )
									objects.AddFirst( UpdateObject ); // Put back at the front of the queue to maintain update order

								Thread.Sleep( RETRY_SLEEP_PERIOD_IN_SECONDS * 1000 );
								continue;
							}

							ErrorException = new ObjectException( Exception, Server, UpdateObject.Object, $"""
							                                                                               Retried {MAX_RETRIES} times. Gave up!
							                                                                               {UpdateObject.ObjectDescription}
							                                                                               """ );
						}
						else
						{
							ErrorException = new ObjectException( Exception,
							                                      Server,
							                                      UpdateObject.Object,
							                                      UpdateObject.ObjectDescription );
						}
						break;
					}
					GlobalErrorHandler.LogException( ErrorException );
				}
			}
		}
		finally
		{
			lock( LockObject )
				CarrierLocks.Remove( carrierId );
		}
	}
}