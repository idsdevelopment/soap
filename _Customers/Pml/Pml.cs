﻿#pragma warning disable IDE1006 // Symbol names

using Protocol.Data._Customers.Pml;

// ReSharper disable InconsistentNaming

namespace Ids.Soap;

public partial interface ISoapService
{
	[OperationContract]
	public Task<remoteTripDetailed[]> pmlFindTripsDetailedUpdatedSince( string authToken,
	                                                                    string zones,
	                                                                    string allKey,
	                                                                    int lowStatus,
	                                                                    int upperStatus,
	                                                                    string serviceLevels,
	                                                                    long sinceSeconds );
}

public partial class SoapService : ISoapService
{
	public async Task<remoteTripDetailed[]> pmlFindTripsDetailedUpdatedSince( string authToken, string zones, string allKey, int lowStatus, int upperStatus, string serviceLevels, long sinceSeconds )
	{
		if( Login( authToken ).Result.Ok )
		{
			var Result = await Azure.Client.RequestPML_FindTripsDetailedUpdatedSince( new PmlFindSince
			                                                                          {
				                                                                          FromStatus    = lowStatus.FromIds1Status().Status,
				                                                                          ToStatus      = upperStatus.FromIds1Status().Status,
				                                                                          ServiceLevels = serviceLevels,
				                                                                          SinceSeconds  = sinceSeconds,
				                                                                          AllZones      = allKey == "ALL",
				                                                                          Zones         = zones
			                                                                          } );

			if( Result is not null )
			{
				return ( from Rd in Result
				         select new remoteTripDetailed( Rd ) ).ToArray();
			}
		}

		return Array.Empty<remoteTripDetailed>();
	}
}