﻿namespace Ids.Soap;

public partial interface ISoapService
{
	[OperationContract]
	public Task<string> LoginTest( string authToken );
}

public partial class SoapService : ISoapService
{
	public async Task<string> LoginTest( string authToken )
	{
		var (Ok, _, _, _) = await Login( authToken );
		return Ok ? "OK" : "Login Failed";
	}
}