﻿namespace Ids.Soap;

public partial interface ISoapService
{
	[OperationContract]
	public void ThrowSoapErrorToAzure( string message );
}

public partial class SoapService : ISoapService
{
	public void ThrowSoapErrorToAzure( string message )
	{
	#if DEBUG
		throw new Exception( message );
	#endif
	}
}