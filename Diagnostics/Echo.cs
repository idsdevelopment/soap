﻿namespace Ids.Soap;

public partial interface ISoapService
{
	[OperationContract]
	public string Echo( string authToken, string message );
}

public partial class SoapService : ISoapService
{
	public string Echo( string authToken, string message ) => message;
}